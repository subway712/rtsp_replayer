#ifndef UDP_RECV_THREAD_H
#define UDP_RECV_THREAD_H

struct __udp_recv_thread
{
	pthread_t tid;
	pthread_mutex_t mtx;
	pthread_cond_t cond;
	struct sockaddr_in local_addr;
	struct sockaddr_in remote_addr;
	int sockfd;
	int thread_state;
	func_t callback;
};
typedef struct __udp_recv_thread udp_recv_thread_t;

//8K
#define RTP_MAX_LENGTH 8192

#define STATE_IDLE 0
#define STATE_RUNNING 1

static bool is_running;

udp_recv_thread_t *udp_recv_thread_create();
void udp_recv_thread_set_local_addr(udp_recv_thread_t *th, struct in_addr local_addr, int local_port);
void udp_recv_thread_set_remote_addr(udp_recv_thread_t *th, struct in_addr remote_addr, int remote_port);
void udp_recv_thread_set_callback(udp_recv_thread_t *th, func_t callback);
void udp_recv_thread_start(udp_recv_thread_t *th);
static void *udp_recv_thread_func(void *args);

//static char bytebuffer[RTP_MAX_LENGTH];

#endif

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "pub.h"
#include "udp_send_thread.h"

udp_send_thread_t *udp_send_thread_create()
{
	udp_send_thread_t *th = (udp_send_thread_t *)malloc(sizeof(udp_send_thread_t));
	pthread_mutex_init(&th->mtx, NULL);
	pthread_cond_init(&th->cond, NULL);
	th->thread_state = STATE_IDLE;

	is_running = true;

	pthread_create(&th->tid, NULL, udp_send_thread_func, th);

	printf("udp_send_thread_create: (%lx)\n", th->tid);
	return th;
}

void udp_send_thread_set_local_addr(udp_send_thread_t *th, struct in_addr local_addr, int local_port){
	//printf("udp_send_thread_start: (%lx) local:%s:%d\n", th->tid, local_ip, local_port);

	th->local_addr.sin_family = AF_INET;
	//inet_pton(AF_INET, local_ip, &th->local_addr.sin_addr);
	th->local_addr.sin_addr = local_addr;
	th->local_addr.sin_port = htons(local_port);
}

//void udp_send_thread_set_remote_addr(udp_send_thread_t *th, const char* remote_ip, int remote_port){
void udp_send_thread_set_remote_addr(udp_send_thread_t *th, struct in_addr remote_addr, int remote_port){
	//printf("udp_send_thread_start: (%lx) remote:%s:%d\n", th->tid, remote_ip, remote_port);

	th->remote_addr.sin_family = AF_INET;
	//inet_pton(AF_INET, remote_ip, &th->remote_addr.sin_addr);
	th->remote_addr.sin_addr = remote_addr;
	th->remote_addr.sin_port = htons(remote_port);
}

void udp_send_thread_set_callback(udp_send_thread_t *th, func_t callback)
{
	printf("udp_send_thread_callback: (%lx)\n", th->tid);

	th->callback = callback;
}

void udp_send_thread_start(udp_send_thread_t *th)
{
	printf("udp_send_thread_start: (%lx)\n", th->tid);

	th->thread_state = STATE_RUNNING;
	pthread_mutex_lock(&th->mtx);
	pthread_cond_signal(&th->cond);
	pthread_mutex_unlock(&th->mtx);
}

void udp_send_thread_stop(udp_send_thread_t *th)
{
	th->thread_state = STATE_IDLE;
	shutdown(th->sockfd, SHUT_RDWR);
	close(th->sockfd);
}

void udp_send_thread_free(udp_send_thread_t *th)
{
	printf("udp_send_thread_free: %lx\n", th->tid);
	pthread_cancel(th->tid);
	pthread_join(th->tid, NULL);
	free(th);
}

static void *udp_send_thread_func(void *args)
{
	udp_send_thread_t *th = (udp_send_thread_t*) args;
	while(is_running)
	{
		switch (th->thread_state)
		{
			case STATE_IDLE:
				printf("(%lx) STATE_IDLE\n", th->tid);
				pthread_mutex_lock(&th->mtx);
				pthread_cond_wait(&th->cond, &th->mtx);
				pthread_mutex_unlock(&th->mtx);
				break;
			case STATE_RUNNING:
				printf("(%lx) STATE_RUNNING\n", th->tid);
				printf("local:%s:%d\n", inet_ntoa(th->local_addr.sin_addr), htons(th->local_addr.sin_port));
				th->sockfd = socket(AF_INET, SOCK_DGRAM, 0);
				int ret = bind(th->sockfd, (struct sockaddr*)&th->local_addr, sizeof(th->local_addr));
				if (ret < 0)
				{
					printf("bind error:%d\n", ret);
					sleep(1);
					break;
				}
				while(is_running)
				{
					//此处应有队列,当回调告知有可发消息时,从队列里读出所有要发的包,之后自锁.
					pthread_mutex_lock(&th->mtx);
					pthread_cond_wait(&th->cond, &th->mtx);

					//int real_len = sendto(th->sockfd, bytebuffer, strlen(bytebuffer), 0,
					//		(struct sockaddr *)&th->remote_addr, sizeof(th->remote_addr));
					//if (real_len < 0)
					//{
					//	printf("recvfrom error. Is socket close?\n");
					//	break;
					//} else if (real_len == 0) {
					//	continue;
					//}
					pthread_mutex_unlock(&th->mtx);
				}
		}
	}
	return NULL;
}

void send_rtp_packet(udp_send_thread_t *th, char *str, int size)
{
	if (th->sockfd != 0)
	{
		//memcpy(bytebuffer, str, size);
		//int real_len = sendto(th->sockfd, bytebuffer, strlen(bytebuffer), 0,
		int real_len = sendto(th->sockfd, str, size, 0,
				(struct sockaddr *)&th->remote_addr, sizeof(th->remote_addr));
		if (real_len < 0)
		{
			printf("recvfrom error. Is socket close?\n");
		}
	}
}

//void callback_test()
//{
//	printf("this is a callback!\n");
//}
//
//int main(int argc, char const *argv[])
//{
//	if(argc != 5)
//	{
//		printf("Usage: %s [local IP] [local port] [remote IP] [remote port]\n", argv[0]);
//		return -1;
//	}
//
//	udp_send_thread_t *th = udp_send_thread_create();
//	udp_send_thread_set_local_addr(th, argv[1], atoi(argv[2]));
//	udp_send_thread_set_remote_addr(th, argv[3], atoi(argv[4]));
//	udp_send_thread_set_callback(th, callback_test);
//
//	udp_send_thread_start(th);
//
//	while(1);
//	//char c;
//	//do {
//	//	printf("s:stop r:restart q:quit\n");
//	//	c = getchar();
//	//	if (c == 's')
//	//	{
//	//		udp_send_thread_stop(th);
//	//	} else if (c == 'r') {
//	//		udp_send_thread_start(th);
//	//	}
//	//} while(c != 'q');
//	//udp_send_thread_free(th);
//	//printf("Is thread free successed? y for yes.\n");
//	//while(getchar() != 'y') {}
//	return 0;
//}

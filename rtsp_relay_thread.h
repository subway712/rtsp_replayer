#ifndef RTSP_RELAY_THREAD_H
#define RTSP_RELAY_THREAD_H

struct __rtsp_relay_thread
{
	pthread_t server_tid;
	pthread_t client_tid;
	pthread_mutex_t server_mtx;
	pthread_mutex_t client_mtx;
	pthread_cond_t server_cond;
	pthread_cond_t client_cond;
	struct sockaddr_in server_addr;
	struct sockaddr_in remote_addr;
	struct sockaddr_in client_addr;
	struct sockaddr_in camera_addr;
	int server_sockfd;
	int server_connfd;
	int client_sockfd;
	int client_connfd;
	int server_thread_state;
	int client_thread_state;
	func_t callback;
};
typedef struct __rtsp_relay_thread rtsp_relay_thread_t;

//8K
#define RTP_MAX_LENGTH 8192

#define STATE_IDLE 0
#define STATE_RUNNING 1

static bool is_running;

static void *rtsp_relay_thread_server_func(void *args);
static void *rtsp_relay_thread_client_func(void *args);

static char server_bytebuffer[RTP_MAX_LENGTH];
static char server_send_bytebuffer[RTP_MAX_LENGTH];

static char client_bytebuffer[RTP_MAX_LENGTH];
static char client_send_bytebuffer[RTP_MAX_LENGTH];

static char local_uri[22];
static char camera_uri[22];
static char server_addr[16];
static char camera_addr[16];

#endif


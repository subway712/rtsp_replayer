#ifndef PUB_H
#define PUB_H

//用作布尔类型判断
#ifndef bool
#define true 1
#define false 0
#define bool int
#endif

//函数指针,用作回调
typedef void (*func_t)(char *str, int size);

#endif

.SUFFIXES : .x .o .c .s

CC := arm-linux-gcc
LD := arm-linux-gcc

STRIP := arm-linux-strip

TARGET = rtsp-proxy

SRCS := $(wildcard *.c)
OBJS := $(patsubst %.c, %.o, $(SRCS))
#OBJS := $(SRCS:%.c=%.o)
BUILD := $(OBJS:%.o=%)



LDFLAGS = -L../../../../../output/target/usr/lib/
LDFLAGS += -lpthread -lzlog -lc -lgcc

CFLAGS = -I../../../../../output/host/usr/arm-nuvoton-linux-uclibcgnueabi/sysroot/usr/include
CFLAGS +=  -Wall 

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^
#	$(STRIP) $@

%o: %c
	$(CC) $(CFLAGS) -o $@ $< 

.PHONY: all clean

clean:
	rm -f $(TARGET)
	rm -f $(BUILD)

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "pub.h"
#include "md5.h"
#include "rtsp_relay_thread.h"
#include "udp_recv_thread.h"
#include "udp_send_thread.h"

#include <zlog.h>

udp_recv_thread_t *recv_th1, *recv_th2, *recv_th3, *recv_th4;
udp_send_thread_t *send_th1, *send_th2, *send_th3, *send_th4;
int track_id;

int find_pos(const char *haystack, const char *needle)
{
	char *pos = strstr(haystack, needle);
	if(pos == NULL) return -1;

	if(pos == haystack)
	{
		return 0;
	}
	return (pos - haystack) / sizeof(char);
}

//将src中的target替换成replacement
//[aaa][target][bbb] -> [aaa][replacement][bbb]
int replace(char *src, int src_size, const char *target, const char *replacement)
{
	//printf("src:%s, target:%s, replacement:%s\n", src, target, replacement);
	//printf("target:%s, replacement:%s\n", target, replacement);
	int size = src_size;
	int target_size = strlen(target);
	int replacement_size = strlen(replacement);
	int pos;
	while((pos = find_pos(src, target)) >= 0)
	{
		if(pos > src_size)
		{
			break;
		}
		memmove(src + pos + replacement_size, src + pos + target_size, size - pos - target_size);
		memcpy(src + pos, replacement, replacement_size);
		size = size - target_size + replacement_size;
		//printf("replace: %d, %s\n", pos, src);
	}
	return size;
}

rtsp_relay_thread_t *rtsp_relay_thread_create()
{
	rtsp_relay_thread_t *th = (rtsp_relay_thread_t *)malloc(sizeof(rtsp_relay_thread_t));
	pthread_mutex_init(&th->server_mtx, NULL);
	pthread_cond_init(&th->server_cond, NULL);
	th->server_thread_state = STATE_IDLE;

	pthread_mutex_init(&th->client_mtx, NULL);
	pthread_cond_init(&th->client_cond, NULL);
	th->client_thread_state = STATE_IDLE;

	is_running = true;

	pthread_create(&th->server_tid, NULL, rtsp_relay_thread_server_func, th);
	pthread_create(&th->client_tid, NULL, rtsp_relay_thread_client_func, th);

	printf("rtsp_relay_thread_create: (%lx)\n", th->server_tid);
	printf("rtsp_relay_thread_create: (%lx)\n", th->client_tid);
	return th;
}

void rtsp_relay_thread_set_server_addr(rtsp_relay_thread_t *th, const char* local_ip, int server_port){
	printf("rtsp_relay_thread_start: (%lx) local:%s:%d\n", th->server_tid, local_ip, server_port);

	th->server_addr.sin_family = AF_INET;
	inet_pton(AF_INET, local_ip, &th->server_addr.sin_addr);
	th->server_addr.sin_port = htons(server_port);
}

void rtsp_relay_thread_set_remote_addr(rtsp_relay_thread_t *th, const char* remote_ip, int remote_port){
	printf("rtsp_relay_thread_start: (%lx) remote:%s:%d\n", th->server_tid, remote_ip, remote_port);

	th->remote_addr.sin_family = AF_INET;
	inet_pton(AF_INET, remote_ip, &th->remote_addr.sin_addr);
	th->remote_addr.sin_port = htons(remote_port);
}

void rtsp_relay_thread_set_client_addr(rtsp_relay_thread_t *th, const char* client_ip, int client_port){
	printf("rtsp_relay_thread_start: (%lx) client:%s:%d\n", th->server_tid, client_ip, client_port);

	th->client_addr.sin_family = AF_INET;
	inet_pton(AF_INET, client_ip, &th->client_addr.sin_addr);
	th->client_addr.sin_port = htons(client_port);
}

void rtsp_relay_thread_set_camera_addr(rtsp_relay_thread_t *th, const char* camera_ip, int camera_port){
	printf("rtsp_relay_thread_start: (%lx) camera:%s:%d\n", th->server_tid, camera_ip, camera_port);

	th->camera_addr.sin_family = AF_INET;
	inet_pton(AF_INET, camera_ip, &th->camera_addr.sin_addr);
	th->camera_addr.sin_port = htons(camera_port);
}

void rtsp_relay_thread_set_callback(rtsp_relay_thread_t *th, func_t callback)
{
	printf("rtsp_relay_thread_callback: (%lx)\n", th->server_tid);

	th->callback = callback;
}

void rtsp_relay_thread_start(rtsp_relay_thread_t *th)
{
	printf("rtsp_relay_thread_start: (%lx)\n", th->server_tid);

	th->server_thread_state = STATE_RUNNING;
	pthread_mutex_lock(&th->server_mtx);
	pthread_cond_signal(&th->server_cond);
	pthread_mutex_unlock(&th->server_mtx);
}

void rtsp_relay_thread_stop(rtsp_relay_thread_t *th)
{
	th->server_thread_state = STATE_IDLE;
	shutdown(th->server_sockfd, SHUT_RDWR);
	close(th->server_sockfd);
	close(th->server_connfd);
	shutdown(th->client_sockfd, SHUT_RDWR);
	close(th->client_sockfd);
	close(th->client_connfd);
}

void rtsp_relay_thread_free(rtsp_relay_thread_t *th)
{
	printf("rtsp_relay_thread_free: %lx\n", th->server_tid);
	pthread_cancel(th->server_tid);
	pthread_join(th->server_tid, NULL);
	printf("rtsp_relay_thread_free: %lx\n", th->client_tid);
	pthread_cancel(th->client_tid);
	pthread_join(th->client_tid, NULL);
	free(th);
}

void th1_send_msg(char *str, int size)
{
	send_rtp_packet(send_th1, str, size);
}

void th2_send_msg(char *str, int size)
{
	send_rtp_packet(send_th2, str, size);
}

void th3_send_msg(char *str, int size)
{
	send_rtp_packet(send_th3, str, size);
}

void th4_send_msg(char *str, int size)
{
	send_rtp_packet(send_th4, str, size);
}

static void *rtsp_relay_thread_server_func(void *args)
{
	rtsp_relay_thread_t *th = (rtsp_relay_thread_t*) args;
	while(is_running)
	{
		switch (th->server_thread_state)
		{
			case STATE_IDLE:
				printf("(%lx) STATE_IDLE\n", th->server_tid);
				pthread_mutex_lock(&th->server_mtx);
				pthread_cond_wait(&th->server_cond, &th->server_mtx);
				pthread_mutex_unlock(&th->server_mtx);
				break;
			case STATE_RUNNING:
				printf("(%lx) STATE_RUNNING\n", th->server_tid);
				th->server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
				int ret = bind(th->server_sockfd, (struct sockaddr*)&th->server_addr, sizeof(th->server_addr));
				if (ret < 0)
				{
					printf("bind error\n");
					break;
				}
				static int BACKLOG = 5;
				listen(th->server_sockfd, BACKLOG);
				socklen_t len = sizeof(struct sockaddr_in);
				printf("waiting for connect...\n");
				th->server_connfd = accept(th->server_sockfd, (struct sockaddr *)&th->remote_addr, &len);
				printf("new connection: %s:%d\n", inet_ntoa(th->remote_addr.sin_addr), htons(th->remote_addr.sin_port));

				//启动client线程
				th->client_thread_state = STATE_RUNNING;
				pthread_mutex_lock(&th->client_mtx);
				pthread_cond_signal(&th->client_cond);
				pthread_mutex_unlock(&th->client_mtx);

				pthread_mutex_lock(&th->server_mtx);
				pthread_cond_wait(&th->server_cond, &th->server_mtx);
				pthread_mutex_unlock(&th->server_mtx);
				if(th->server_thread_state != STATE_RUNNING)
				{
					break;
				}

				//开始接收RTSP流
				bzero(server_bytebuffer, RTP_MAX_LENGTH);
				bzero(server_send_bytebuffer, RTP_MAX_LENGTH);
				int size;
				while(is_running)
				{
					size = read(th->server_connfd, server_bytebuffer, RTP_MAX_LENGTH);
					if(size < 0)
					{
						rtsp_relay_thread_stop(th);
						break;
					} else if (size == 0) {
						continue;
					}
					printf("server recv: %s\n", server_bytebuffer);
					memcpy(server_send_bytebuffer, server_bytebuffer, size);
					bzero(server_bytebuffer, size);

					//替换内容中的uri
					size = replace(server_send_bytebuffer, size, local_uri, camera_uri);
					size = replace(server_send_bytebuffer, size, server_addr, camera_addr);

					//进行鉴权计算
					if (find_pos(server_send_bytebuffer, "Authorization") >= 0)
					{
						char *method;
						if (strstr(server_send_bytebuffer, "DESCRIBE") != NULL)
						{
							method = "DESCRIBE";
						} else if (strstr(server_send_bytebuffer, "SETUP") != NULL) {
							method = "SETUP";
						} else if (strstr(server_send_bytebuffer, "PLAY") != NULL) {
							method = "PLAY";
						} else if (strstr(server_send_bytebuffer, "GET_PARAMETER") != NULL) {
							method = "GET_PARAMETER";
						} else if (strstr(server_send_bytebuffer, "TEARDOWN") != NULL) {
							method = "TEARDOWN";
						} else {
							method = "";
						}
						char *password = "tcb712123";
						char *username = (char *)malloc(32 * sizeof(char));
						char *realm = (char *)malloc(32 * sizeof(char));
						char *nonce = (char *)malloc(32 * sizeof(char));
						char *uri = (char *)malloc(64 * sizeof(char));
						char *response = (char *)malloc(32 * sizeof(char));
						sscanf(strstr(server_send_bytebuffer, "username="), "username=\"%[^\r\n\"]\"", username);
						sscanf(strstr(server_send_bytebuffer, "realm="), "realm=\"%[^\r\n\"]\"", realm);
						sscanf(strstr(server_send_bytebuffer, "nonce="), "nonce=\"%[^\r\n\"]\"", nonce);
						sscanf(strstr(server_send_bytebuffer, "uri="), "uri=\"%[^\r\n\"]\"", uri);
						sscanf(strstr(server_send_bytebuffer, "response="), "response=\"%[^\r\n\"]\"", response);
						//printf("username=%s\n", username);
						//printf("realm=%s\n", realm);
						//printf("nonce=%s\n", nonce);
						//printf("uri=%s\n", uri);
						//printf("response=%s\n", response);
						char *str1 = (char *)malloc(128 * sizeof(char));
						char *str2 = (char *)malloc(128 * sizeof(char));
						char *str3 = (char *)malloc(128 * sizeof(char));
						char *md5_1 = (char *)malloc(32 * sizeof(char));
						char *md5_2 = (char *)malloc(32 * sizeof(char));
						char *md5_3 = (char *)malloc(32 * sizeof(char));
						sprintf(str1, "%s:%s:%s", username, realm, password);
						Compute_string_md5((unsigned char*)str1, strlen(str1), md5_1);
						sprintf(str2, "%s:%s", method, uri);
						Compute_string_md5((unsigned char*)str2, strlen(str2), md5_2);
						sprintf(str3, "%s:%s:%s", md5_1, nonce, md5_2);
						Compute_string_md5((unsigned char*)str3, strlen(str3), md5_3);
						size = replace(server_send_bytebuffer, size, response, md5_3);
						free(username);
						free(realm);
						free(nonce);
						free(uri);
						free(response);
						free(str1);
						free(str2);
						free(str3);
						free(md5_1);
						free(md5_2);
						free(md5_3);
					}

					printf("server send: %s\n", server_send_bytebuffer);
					write(th->client_sockfd, server_send_bytebuffer, size);

					bzero(server_send_bytebuffer, size);
				}
				break;
		}
	}
	return NULL;
}

static void *rtsp_relay_thread_client_func(void *args)
{
	rtsp_relay_thread_t *th = (rtsp_relay_thread_t*) args;
	while(is_running)
	{
		switch (th->client_thread_state)
		{
			case STATE_IDLE:
				printf("(%lx) STATE_IDLE\n", th->client_tid);
				pthread_mutex_lock(&th->client_mtx);
				pthread_cond_wait(&th->client_cond, &th->client_mtx);
				pthread_mutex_unlock(&th->client_mtx);
				break;
			case STATE_RUNNING:
				printf("(%lx) STATE_RUNNING\n", th->client_tid);
				printf("client local:%s:%d\n", inet_ntoa(th->client_addr.sin_addr), htons(th->client_addr.sin_port));

				//启动client用作对摄像头的拉流
				th->client_sockfd = socket(AF_INET, SOCK_STREAM, 0);
				int ret = bind(th->client_sockfd, (struct sockaddr*)&th->client_addr, sizeof(th->client_addr));
				if (ret < 0)
				{
					printf("bind error\n");
					break;
				}
				printf("try to connect to camera...\n");
				connect(th->client_sockfd, (struct sockaddr *)&th->camera_addr, sizeof(th->camera_addr));
				printf("connect to camera successed.\n");

				//通知server已启动
				pthread_mutex_lock(&th->server_mtx);
				pthread_cond_signal(&th->server_cond);
				pthread_mutex_unlock(&th->server_mtx);

				//开始接收RTSP流
				bzero(client_bytebuffer, RTP_MAX_LENGTH);
				int size;
				while(is_running)
				{
					size = read(th->client_sockfd, client_bytebuffer, RTP_MAX_LENGTH);
					if(size < 0)
					{
						rtsp_relay_thread_stop(th);
						break;
					} else if (size == 0) {
						continue;
					}

					printf("client recv: %s\nsize:%d\n", client_bytebuffer, size);
					memcpy(client_send_bytebuffer, client_bytebuffer, size);
					bzero(client_bytebuffer, size);

					if(strstr(client_send_bytebuffer, "Content-Length: ") != NULL)
					{
						printf("_______________find Content-Length:________________\n");
						int sdp_length = 0;
						int start_pos = find_pos(client_send_bytebuffer, "Content-Length: ");
						sscanf(client_send_bytebuffer + start_pos, "Content-Length: %d", &sdp_length);
						int old_size = size;
						char *old_str = (char *)malloc(32 * sizeof(char));
						sprintf(old_str, "Content-Length: %d", sdp_length);
						printf("old_str: %s\n",old_str);

						size = replace(client_send_bytebuffer + start_pos, size, camera_uri, local_uri);
						size = replace(client_send_bytebuffer + start_pos, size, camera_addr, server_addr);

						sdp_length = sdp_length + size - old_size;
						char *new_str = (char *)malloc(32 * sizeof(char));
						sprintf(new_str, "Content-Length: %d", sdp_length);
						printf("new_str: %s\n",new_str);
						size = replace(client_send_bytebuffer + start_pos, size, old_str, new_str);
						free(old_str);
						free(new_str);
					}
					size = replace(client_send_bytebuffer, size, camera_uri, local_uri);

					if(strstr(client_send_bytebuffer, "client_port=") != NULL)
					{
						//track_id
						int client_port1, client_port2, server_port1, server_port2;
						sscanf(strstr(client_send_bytebuffer, "client_port="), "client_port=%d-%d", &client_port1, &client_port2);
						sscanf(strstr(client_send_bytebuffer, "server_port="), "server_port=%d-%d", &server_port1, &server_port2);
						printf("track_id:%d\n", track_id);
						printf("client_port:%d,%d\n", client_port1, client_port2);
						printf("server_port:%d,%d\n", server_port1, server_port2);
						if (track_id == 0)
						{
							track_id = 1;
							udp_recv_thread_set_local_addr(recv_th1, th->client_addr.sin_addr, client_port1);
							udp_recv_thread_set_remote_addr(recv_th1, th->camera_addr.sin_addr, server_port1);
							udp_recv_thread_set_callback(recv_th1, th1_send_msg);
							udp_recv_thread_start(recv_th1);
							udp_recv_thread_set_local_addr(recv_th2, th->client_addr.sin_addr, client_port2);
							udp_recv_thread_set_remote_addr(recv_th2, th->camera_addr.sin_addr, server_port2);
							udp_recv_thread_set_callback(recv_th2, th2_send_msg);
							udp_recv_thread_start(recv_th2);
							udp_send_thread_set_local_addr(send_th1, th->server_addr.sin_addr, server_port1);
							udp_send_thread_set_remote_addr(send_th1, th->remote_addr.sin_addr, client_port1);
							udp_send_thread_start(send_th1);
							udp_send_thread_set_local_addr(send_th2, th->server_addr.sin_addr, server_port2);
							udp_send_thread_set_remote_addr(send_th2, th->remote_addr.sin_addr, client_port2);
							udp_send_thread_start(send_th2);
						} else {
							track_id = 0;
							udp_recv_thread_set_local_addr(recv_th3, th->client_addr.sin_addr, client_port1);
							udp_recv_thread_set_remote_addr(recv_th3, th->camera_addr.sin_addr, server_port1);
							udp_recv_thread_set_callback(recv_th3, th3_send_msg);
							udp_recv_thread_start(recv_th3);
							udp_recv_thread_set_local_addr(recv_th4, th->client_addr.sin_addr, client_port2);
							udp_recv_thread_set_remote_addr(recv_th4, th->camera_addr.sin_addr, server_port2);
							udp_recv_thread_set_callback(recv_th4, th4_send_msg);
							udp_recv_thread_start(recv_th4);
							udp_send_thread_set_local_addr(send_th3, th->server_addr.sin_addr, server_port1);
							udp_send_thread_set_remote_addr(send_th3, th->remote_addr.sin_addr, client_port1);
							udp_send_thread_start(send_th3);
							udp_send_thread_set_local_addr(send_th4, th->server_addr.sin_addr, server_port2);
							udp_send_thread_set_remote_addr(send_th4, th->remote_addr.sin_addr, client_port2);
							udp_send_thread_start(send_th4);
						}
					}

					printf("client send: %s\nsize:%d\n", client_send_bytebuffer, size);
					write(th->server_connfd, client_send_bytebuffer, size);
					bzero(client_send_bytebuffer, size);
				}
				break;
		}
	}
	return NULL;
}

int main(int argc, char const *argv[])
{
	int rc;
	zlog_category_t *zc;
	rc = zlog_init("./zlog.conf");
	if (rc) {
		printf("zlog init failed");
	}
	zc=zlog_get_category("*.*");
	zlog_info(zc,"hello");

	



	if(argc != 10)
	{
		printf("Usage: %s [server IP] [port] [remote IP] [port] [client IP] [port] [camera IP] [port] [xindaoji IP]\n", argv[0]);
		return -1;
	}
	track_id = 0;

	rtsp_relay_thread_t *th = rtsp_relay_thread_create();
	rtsp_relay_thread_set_server_addr(th, argv[1], atoi(argv[2]));
	rtsp_relay_thread_set_remote_addr(th, argv[3], atoi(argv[4]));
	rtsp_relay_thread_set_client_addr(th, argv[5], atoi(argv[6]));
	rtsp_relay_thread_set_camera_addr(th, argv[7], atoi(argv[8]));
	sprintf(local_uri, "%s:%d", argv[9], htons(th->server_addr.sin_port));
	sprintf(camera_uri, "%s:%d", inet_ntoa(th->camera_addr.sin_addr), htons(th->camera_addr.sin_port));
	sprintf(server_addr, "%s", argv[9]);
	sprintf(camera_addr, "%s", inet_ntoa(th->camera_addr.sin_addr));

	recv_th1 = udp_recv_thread_create();
	recv_th2 = udp_recv_thread_create();
	recv_th3 = udp_recv_thread_create();
	recv_th4 = udp_recv_thread_create();
	send_th1 = udp_send_thread_create();
	send_th2 = udp_send_thread_create();
	send_th3 = udp_send_thread_create();
	send_th4 = udp_send_thread_create();

	char c;
	do {
		printf("s:stop r:restart q:quit\n");
		c = getchar();
		if (c == 's')
		{
			rtsp_relay_thread_stop(th);
		} else if (c == 'r') {
			rtsp_relay_thread_start(th);
		} else if (c == EOF) {
			printf("EOF?\n");
			break;
		} else {
			printf("c:%d\n", c);
		}
	} while(c != 'q');
	rtsp_relay_thread_free(th);
	printf("Is thread free successed? y for yes.\n");
	while(getchar() != 'y') {}
	
	zlog_fini();
	return 0;
}

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "pub.h"
#include "udp_recv_thread.h"

udp_recv_thread_t *udp_recv_thread_create()
{
	udp_recv_thread_t *th = (udp_recv_thread_t *)malloc(sizeof(udp_recv_thread_t));
	pthread_mutex_init(&th->mtx, NULL);
	pthread_cond_init(&th->cond, NULL);
	th->thread_state = STATE_IDLE;

	is_running = true;

	pthread_create(&th->tid, NULL, udp_recv_thread_func, th);

	printf("udp_recv_thread_create: (%lx)\n", th->tid);
	return th;
}

void udp_recv_thread_set_local_addr(udp_recv_thread_t *th, struct in_addr local_addr, int local_port){
	//printf("udp_recv_thread_start: (%lx) local:%s:%d\n", th->tid, local_ip, local_port);

	th->local_addr.sin_family = AF_INET;
	//inet_pton(AF_INET, local_ip, &th->local_addr.sin_addr);
	th->local_addr.sin_addr = local_addr;
	th->local_addr.sin_port = htons(local_port);
}

//void udp_recv_thread_set_remote_addr(udp_recv_thread_t *th, const char* remote_ip, int remote_port){
void udp_recv_thread_set_remote_addr(udp_recv_thread_t *th, struct in_addr remote_addr, int remote_port){
	//printf("udp_recv_thread_start: (%lx) remote:%s:%d\n", th->tid, remote_ip, remote_port);

	th->remote_addr.sin_family = AF_INET;
	//inet_pton(AF_INET, remote_ip, &th->remote_addr.sin_addr);
	th->remote_addr.sin_addr = remote_addr;
	th->remote_addr.sin_port = htons(remote_port);
}

void udp_recv_thread_set_callback(udp_recv_thread_t *th, func_t callback)
{
	printf("udp_recv_thread_callback: (%lx)\n", th->tid);

	th->callback = callback;
}

void udp_recv_thread_start(udp_recv_thread_t *th)
{
	printf("udp_recv_thread_start: (%lx)\n", th->tid);

	th->thread_state = STATE_RUNNING;
	pthread_mutex_lock(&th->mtx);
	pthread_cond_signal(&th->cond);
	pthread_mutex_unlock(&th->mtx);
}

void udp_recv_thread_stop(udp_recv_thread_t *th)
{
	th->thread_state = STATE_IDLE;
	shutdown(th->sockfd, SHUT_RDWR);
	close(th->sockfd);
}

void udp_recv_thread_free(udp_recv_thread_t *th)
{
	printf("udp_recv_thread_free: %lx\n", th->tid);
	pthread_cancel(th->tid);
	pthread_join(th->tid, NULL);
	free(th);
}

static void *udp_recv_thread_func(void *args)
{
	udp_recv_thread_t *th = (udp_recv_thread_t*) args;
	while(is_running)
	{
		switch (th->thread_state)
		{
			case STATE_IDLE:
				printf("(%lx) STATE_IDLE\n", th->tid);
				pthread_mutex_lock(&th->mtx);
				pthread_cond_wait(&th->cond, &th->mtx);
				pthread_mutex_unlock(&th->mtx);
				break;
			case STATE_RUNNING:
				printf("(%lx) STATE_RUNNING\n", th->tid);
				printf("local:%s:%d\n", inet_ntoa(th->local_addr.sin_addr), htons(th->local_addr.sin_port));
				th->sockfd = socket(AF_INET, SOCK_DGRAM, 0);
				int ret = bind(th->sockfd, (struct sockaddr*)&th->local_addr, sizeof(th->local_addr));
				if (ret < 0)
				{
					printf("bind error\n");
					sleep(1);
					break;
				}
				while(is_running)
				{
					struct sockaddr_in remote_addr;
					socklen_t len = sizeof(remote_addr);
					char bytebuffer[RTP_MAX_LENGTH];
					int real_len = recvfrom(th->sockfd, bytebuffer, sizeof(bytebuffer), 0, (struct sockaddr*)&remote_addr, &len);
					if (real_len < 0)
					{
						printf("recvfrom error. Is socket close?\n");
						break;
					} else if (real_len == 0) {
						continue;
					}
					//读到了数据
					//printf("---\n");
					//printf("content: %s\n", bytebuffer);
					//printf("addr: %s:%d\n", inet_ntoa(remote_addr.sin_addr), htons(remote_addr.sin_port));
					//printf("len: %d\n", real_len);
					//printf("port: %d\n", ntohs(th->local_addr.sin_port));
					//if (remote_addr.sin_port != th->remote_addr.sin_port)
					//{
					//	printf("neq!\n");
					//} else {
					//	th->callback(bytebuffer, real_len);
					//}
					th->callback(bytebuffer, real_len);
				}
				break;
		}
	}
	return NULL;
}

//void callback_test(char *str, int size)
//{
//	printf("this is a callback!\n");
//}
//
//int main(int argc, char const *argv[])
//{
//	if(argc != 5)
//	{
//		printf("Usage: %s [local IP] [local port] [remote IP] [remote port]\n", argv[0]);
//		return -1;
//	}
//
//	udp_recv_thread_t *th = udp_recv_thread_create();
//	struct in_addr local_addr;
//	inet_pton(AF_INET, argv[1], &local_addr);
//	udp_recv_thread_set_local_addr(th, local_addr, atoi(argv[2]));
//	struct in_addr remote_addr;
//	inet_pton(AF_INET, argv[3], &remote_addr);
//	udp_recv_thread_set_remote_addr(th, remote_addr, atoi(argv[4]));
//	udp_recv_thread_set_callback(th, callback_test);
//
//	char c;
//	do {
//		printf("s:stop r:restart q:quit\n");
//		c = getchar();
//		if (c == 's')
//		{
//			udp_recv_thread_stop(th);
//		} else if (c == 'r') {
//			udp_recv_thread_start(th);
//		}
//	} while(c != 'q');
//	udp_recv_thread_free(th);
//	printf("Is thread free successed? y for yes.\n");
//	while(getchar() != 'y') {}
//	return 0;
//}
